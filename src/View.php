<?php
/**
 * Created by PhpStorm.
 * User: eska
 * Date: 22.09.18
 * Time: 20:52
 */

namespace KarolSzarafinowski\Togp;

class View
{
    private $context = [];

    public function __construct()
    {
        $this->context = \Timber::get_context();
        $this->autoFields();
    }

    public function __set($name, $value): void
    {
        $this->context[$name] = $value;
    }

    public function __get($name)
    {
        return $this->context[$name];
    }

    public function get(): array
    {
        return $this->context;
    }

    protected function autoFields()
    {
    }

    public function render(array $templates): void
    {
        if (WP_DEBUG) {
            \Timber\Timber::render($templates, $this->context);
        } else {
            ob_start(function ($buffer) {
                // replace all the apples with oranges
                return str_replace(["\n", "\r", "\t"], '', $buffer);
            });

            \Timber\Timber::render($templates, $this->context);
            ob_end_flush();
        }
    }
}