<?php
/**
 * Created by PhpStorm.
 * User: eska
 * Date: 02.09.18
 * Time: 23:03
 */

namespace KarolSzarafinowski\Togp;

class GetPosts
{
    private $offset = 0;
    private $category = '';
    private $category_name = '';
    private $orderby = 'date';
    private $order = 'DESC';
    private $include = '';
    private $exclude = '';
    private $meta_key = '';
    private $meta_value = '';
    private $post_type = 'post';
    private $post_mime_type = '';
    private $post_parent = '';
    private $author = '';
    private $author_name = '';
    private $post_status = 'publish';
    private $suppress_filters = true;
    private $fields = '';
    private $post__not_in = [];

    /**
     * @return GetPosts
     */
    public static function factory()
    {
        return new GetPosts();
    }

    public function all()
    {
        return $this->get(-1);
    }

    public function one()
    {
        return $this->get(1);
    }

    public function get($howMuch, $offset = null)
    {
        return \Timber::get_posts([
            'posts_per_page' => $howMuch,
            'offset' => ($offset === null) ? $this->offset : $offset,
            'category' => $this->category,
            'category_name' => $this->category_name,
            'orderby' => $this->orderby,
            'order' => $this->order,
            'include' => $this->include,
            'exclude' => $this->exclude,
            'meta_key' => $this->meta_key,
            'meta_value' => $this->meta_value,
            'post_type' => $this->post_type,
            'post_mime_type' => $this->post_mime_type,
            'post_parent' => $this->post_parent,
            'author' => $this->author,
            'author_name' => $this->author_name,
            'post_status' => $this->post_status,
            'suppress_filters' => $this->suppress_filters,
            'fields' => $this->fields,
            'post__not_in' => $this->post__not_in
        ]);
    }

    public function setOrderByACF(string $key, string $order = 'ASC')
    {
        $this->setMetaKey($key);
        $this->setOrderby('meta_value');
        $this->setOrder($order);
        return $this;
    }

    /**
     * @return int
     */
    public function getPostsPerPage()
    {
        return $this->posts_per_page;
    }

    /**
     * @param int $posts_per_page
     * @return GetPosts
     */
    public function setPostsPerPage($posts_per_page)
    {
        $this->posts_per_page = $posts_per_page;
        return $this;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     * @return GetPosts
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     * @return GetPosts
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
        return $this->category_name;
    }

    /**
     * @param string $category_name
     * @return GetPosts
     */
    public function setCategoryName($category_name)
    {
        $this->category_name = $category_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderby()
    {
        return $this->orderby;
    }

    /**
     * @param string $orderby
     * @return GetPosts
     */
    public function setOrderby($orderby)
    {
        $this->orderby = $orderby;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param string $order
     * @return GetPosts
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return string
     */
    public function getInclude()
    {
        return $this->include;
    }

    /**
     * @param string $include
     * @return GetPosts
     */
    public function setInclude($include)
    {
        $this->include = $include;
        return $this;
    }

    /**
     * @return string
     */
    public function getExclude()
    {
        return $this->exclude;
    }

    /**
     * @param string $exclude
     * @return GetPosts
     */
    public function setExclude($exclude)
    {
        $this->exclude = $exclude;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaKey()
    {
        return $this->meta_key;
    }

    /**
     * @param string $meta_key
     * @return GetPosts
     */
    public function setMetaKey($meta_key)
    {
        $this->meta_key = $meta_key;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaValue()
    {
        return $this->meta_value;
    }

    /**
     * @param string $meta_value
     * @return GetPosts
     */
    public function setMetaValue($meta_value)
    {
        $this->meta_value = $meta_value;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostType()
    {
        return $this->post_type;
    }

    /**
     * @param string $post_type
     * @return GetPosts
     */
    public function setPostType($post_type)
    {
        $this->post_type = $post_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostMimeType()
    {
        return $this->post_mime_type;
    }

    /**
     * @param string $post_mime_type
     * @return GetPosts
     */
    public function setPostMimeType($post_mime_type)
    {
        $this->post_mime_type = $post_mime_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostParent()
    {
        return $this->post_parent;
    }

    /**
     * @param string $post_parent
     * @return GetPosts
     */
    public function setPostParent($post_parent)
    {
        $this->post_parent = $post_parent;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     * @return GetPosts
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthorName()
    {
        return $this->author_name;
    }

    /**
     * @param string $author_name
     * @return GetPosts
     */
    public function setAuthorName($author_name)
    {
        $this->author_name = $author_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostStatus()
    {
        return $this->post_status;
    }

    /**
     * @param string $post_status
     * @return GetPosts
     */
    public function setPostStatus($post_status)
    {
        $this->post_status = $post_status;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSuppressFilters()
    {
        return $this->suppress_filters;
    }

    /**
     * @param bool $suppress_filters
     * @return GetPosts
     */
    public function setSuppressFilters($suppress_filters)
    {
        $this->suppress_filters = $suppress_filters;
        return $this;
    }

    /**
     * @return string
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param string $fields
     * @return GetPosts
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * @return array
     */
    public function getPostNotIn(): array
    {
        return $this->post__not_in;
    }

    /**
     * @param array $post__not_in
     * @return $this
     */
    public function setPostNotIn(array $post__not_in)
    {
        $this->post__not_in = $post__not_in;
        return $this;
    }

}
